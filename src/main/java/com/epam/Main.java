package com.epam;

import org.apache.logging.log4j.*;

public class Main {

    private static Logger logger1 = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        logger1.trace("Trace");
        logger1.debug("Debug");
        logger1.info("Info");
        logger1.warn("Warn");
        logger1.error("eee Error");
        logger1.fatal("fff Fatal");

    }
}
